"""
SubAlbum class is for folder inside album, examples: CD1, Disc 1, ...
"""

import os
import re

from src.announcer import Announcer
from src.constants import SUB_ALBUM_PATTERN, MUSIC_FILE_EXTENSIONS
from src.helpers import extract_dirname, sanitise_title, split_extension, sleep
from src.monads import listdir, rename, remove
from src.song.subalbumsong import SubAlbumSong


class SubAlbum:
    """
    SubAlbum class is for folder inside album, examples: CD1, Disc 1, ...
    """
    def __init__(self, artists_folder: str, artist_path: str, album: str, sub_album: str):
        self.root = artists_folder

        self.artist_path = artist_path
        self.artist = extract_dirname(artist_path)

        self.album_path = os.path.join(artist_path, album)
        self.album = album

        self.sub_album = sub_album
        self.sub_album_path = os.path.join(self.album_path, sub_album)

    def make_song(self, song: str) -> SubAlbumSong:
        """ Return a song object for sub-album location."""
        return SubAlbumSong(self.root, self.artist, self.album, self.sub_album, song)

    def scan(self) -> None:
        """ Analyze sub-album content """
        Announcer.info(f'scanning {self.sub_album}\n')

        self.__remove_extraneous_files()

        # Check if album must be renamed.
        if not SUB_ALBUM_PATTERN.fullmatch(self.sub_album):
            self._rename()

        for song in listdir(self.sub_album_path):
            song = self.make_song(song)
            song.scan()
            del song

    def _rename(self) -> None:
        Announcer.info(f"Rename '{self.sub_album}' in album '{self.album}' by '{self.artist}'")

        sanitized_sub_album_title = sanitise_title(self.sub_album, self.artist, self.album)

        new_sub_album_title = ' '.join(re.findall(r"[^\d\(\)]+", sanitized_sub_album_title))

        new_path = os.path.join(self.album_path, new_sub_album_title)

        if Announcer.ask_if_rename(self.sub_album_path, new_path):
            rename(self.sub_album_path, new_path)
            self.sub_album_path = new_path
            self.sub_album = new_sub_album_title
        else:
            Announcer.error(f"'{self.sub_album}' in '{self.album}' by '{self.artist}' doesn't match sub-album regex.")
            sleep()

    def __remove_extraneous_files(self) -> None:
        """
        Remove non-song files from directory. User will be prompt before removing.
        """
        for item in listdir(self.sub_album_path):
            item_path = os.path.join(self.sub_album_path, item)

            # handle directory
            if os.path.isdir(item_path):
                Announcer.ask_if_delete(item_path)

            # handle file
            else:
                title, file_extension = split_extension(item)

                if file_extension not in MUSIC_FILE_EXTENSIONS:

                    # music file with extension in uppercase
                    if file_extension.lower() in MUSIC_FILE_EXTENSIONS:
                        new_name = title + '.' + file_extension.lower()
                        new_path = os.path.join(self.sub_album_path, new_name)

                        if Announcer.ask_if_rename(item_path, new_path):
                            rename(item_path, new_path)

                    # not music file
                    else:
                        if Announcer.ask_if_delete(item_path):
                            remove(item_path)
