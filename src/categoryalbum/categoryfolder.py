"""

"""

import os

from src.announcer import Announcer
from src.categoryalbum.categoryalbum import CategoryAlbum
from src.constants import CATEGORY_FOLDER_PATTERN, MUSIC_FILE_EXTENSIONS, ALBUM_PATTERN, SUB_ALBUM_PATTERN
from src.helpers import all_content_is_dir, split_extension, sleep
from src.monads import listdir, rename, remove
from src.song.albumsong import AlbumSong


class CategoryFolder:
    """ """
    def __init__(self, artists_folder: str, artist: str, category_folder: str) -> None:
        self.root = artists_folder

        self.artist = artist
        self.artist_path = os.path.join(self.root, artist)

        self.category_folder = category_folder
        self.category_folder_path = os.path.join(self.artist_path, category_folder)

    def make_song(self, song: str) -> AlbumSong:
        """ """
        return AlbumSong(self.root, self.artist, self.category_folder, song)

    def scan(self):
        """ """
        Announcer.info(f'scanning {self.category_folder}\n')

        self.__remove_extraneous_files()

        # Check if album must be renamed.
        if not CATEGORY_FOLDER_PATTERN.fullmatch(self.category_folder):
            self._rename()

        if all_content_is_dir(self.category_folder_path):
            Announcer.warning(f"All files are directories in album '{self.category_folder}'")
            for album_name in listdir(self.category_folder_path):
                category_album = CategoryAlbum(self.root, self.artist_path, self.category_folder, album_name)
                category_album.scan()
                del category_album
        else:
            for song in listdir(self.category_folder_path):
                song = self.make_song(song)
                song.scan()
                del song

    def _rename(self) -> None:
        """ Rename album filename to standard album name """
        new_title = Announcer.ask_input(self.category_folder)
        if new_title:
            new_category_folder_path = os.path.join(self.artist_path, new_title)
            rename(self.category_folder_path, new_category_folder_path)
            self.category_folder_path = new_category_folder_path
            self.category_folder = new_title
        else:
            Announcer.error(f"'{self.category_folder}' by '{self.artist}' doesn't match category album regex.")
            sleep()

    def __remove_extraneous_files(self) -> None:
        """
        Remove non-song files from directory. User will be prompt before removing.
        """
        for item in listdir(self.category_folder_path):
            item_path = os.path.join(self.category_folder_path, item)

            # handle directory
            if os.path.isdir(item_path):
                if not ALBUM_PATTERN.fullmatch(item) and not SUB_ALBUM_PATTERN.fullmatch(item):
                    Announcer.ask_if_delete(item_path)

            # handle file
            else:
                title, file_extension = split_extension(item)

                if file_extension not in MUSIC_FILE_EXTENSIONS:

                    # music file with extension in uppercase
                    if file_extension.lower() in MUSIC_FILE_EXTENSIONS:

                        new_name = title + '.' + file_extension.lower()
                        new_path = os.path.join(self.category_folder_path, new_name)

                        if Announcer.ask_if_rename(item_path, new_path):
                            rename(item_path, new_path)

                    # not music file
                    else:
                        if Announcer.ask_if_delete(item_path):
                            remove(item_path)
