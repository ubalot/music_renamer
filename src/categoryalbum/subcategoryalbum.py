import os
import re

from src.announcer import Announcer
from src.constants import MUSIC_FILE_EXTENSIONS, SUB_ALBUM_PATTERN
from src.helpers import sanitise_title, split_extension, sleep
from src.monads import listdir, rename, remove
from src.song.subcategoryalbumsong import SubCategoryAlbumSong


class SubCategoryAlbum:
    def __init__(self, artists_folder: str, artist: str, category_folder: str, category_album: str, sub_category_album: str) -> None:
        self.root = artists_folder

        self.artist = artist
        self.artist_path = os.path.join(self.root, artist)

        self.category_folder = category_folder
        self.category_folder_path = os.path.join(self.artist_path, category_folder)

        self.category_album = category_album
        self.category_album_path = os.path.join(self.category_folder_path, category_album)

        self.sub_category_album = sub_category_album
        self.sub_category_album_path = os.path.join(self.category_album_path, sub_category_album)

    def make_song(self, song: str) -> SubCategoryAlbumSong:
        return SubCategoryAlbumSong(self.root,
                                    self.artist,
                                    self.category_folder,
                                    self.category_album,
                                    self.sub_category_album,
                                    song)

    def scan(self) -> None:
        Announcer.info(f'scanning {self.sub_category_album}\n')

        self.__remove_extraneous_files()

        # Check if album must be renamed.
        if not SUB_ALBUM_PATTERN .fullmatch(self.sub_category_album):
            self._rename()

        for song in listdir(self.sub_category_album_path):
            song = self.make_song(song)
            song.scan()
            del song

    def _rename(self) -> None:
        Announcer.info(f"Rename '{self.sub_category_album}' in album '{self.category_album}' in category folder '{self.category_folder}' by '{self.artist}'")

        sanitized_sub_category_album_title = sanitise_title(self.sub_category_album, self.artist, self.category_album)

        new_sub_category_album_title = ' '.join(re.findall(r"[^\d\(\)]+", sanitized_sub_category_album_title))

        new_sub_category_album_path = os.path.join(self.category_folder_path, new_sub_category_album_title)

        if Announcer.ask_if_rename(self.category_folder_path, new_sub_category_album_path):
            rename(self.sub_category_album_path, new_sub_category_album_path)
            self.sub_category_album_path = new_sub_category_album_path
            self.sub_category_album = new_sub_category_album_title
        else:
            Announcer.error(f"'{self.sub_category_album}' in '{self.category_album}' by '{self.artist}' doesn't match sub-album regex.")
            sleep()

    def __remove_extraneous_files(self) -> None:
        """
        Remove non-song files from directory. User will be prompt before removing.
        """
        for item in listdir(self.sub_category_album_path):
            item_path = os.path.join(self.sub_category_album_path, item)

            # handle directory
            if os.path.isdir(item_path):
                Announcer.ask_if_delete(item_path)

            # handle file
            else:
                title, file_extension = split_extension(item)

                if file_extension not in MUSIC_FILE_EXTENSIONS:

                    # music file with extension in uppercase
                    if file_extension.lower() in MUSIC_FILE_EXTENSIONS:
                        new_name = title + '.' + file_extension.lower()
                        new_path = os.path.join(self.sub_category_album_path, new_name)

                        if Announcer.ask_if_rename(item_path, new_path):
                            rename(item_path, new_path)

                    # not music file
                    else:
                        if Announcer.ask_if_delete(item_path):
                            remove(item_path)
