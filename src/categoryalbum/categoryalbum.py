""" """

import os

from src.announcer import Announcer
from src.categoryalbum.subcategoryalbum import SubCategoryAlbum
from src.constants import MUSIC_FILE_EXTENSIONS, ALBUM_PATTERN, SUB_ALBUM_PATTERN
from src.helpers import all_content_is_dir, split_extension
from src.monads import listdir, rename, remove
from src.song.categoryalbumsong import CategoryAlbumSong


class CategoryAlbum:
    """ """

    def __init__(self, artists_folder: str, artist: str, category_folder: str, category_album: str) -> None:
        self.root = artists_folder

        self.artist = artist
        self.artist_path = os.path.join(self.root, artist)

        self.category_folder = category_folder
        self.category_folder_path = os.path.join(self.artist_path, category_folder)

        self.category_album = category_album
        self.category_album_path = os.path.join(self.category_folder_path, category_album)

    def make_song(self, song: str) -> CategoryAlbumSong:
        """ """
        return CategoryAlbumSong(self.root, self.artist, self.category_folder, self.category_album, song)

    def scan(self) -> None:
        """ """
        Announcer.info(f'scanning {self.category_album}\n')

        self.__remove_extraneous_files()

        # Check if album must be renamed.
        if not ALBUM_PATTERN.fullmatch(self.category_album) and not SUB_ALBUM_PATTERN.fullmatch(self.category_album):
            self._rename()

        if all_content_is_dir(self.category_album_path):
            for sub_category_album_name in listdir(self.category_album_path):
                sub_category_album = SubCategoryAlbum(self.root,
                                                      self.artist_path,
                                                      self.category_folder,
                                                      self.category_album,
                                                      sub_category_album_name)
                sub_category_album.scan()
                del sub_category_album
        else:
            for song in listdir(self.category_album_path):
                song = self.make_song(song)
                song.scan()
                del song

    def _rename(self) -> None:
        answer = Announcer.ask_input(f"How would you like to rename {self.category_album} ?")
        if answer:
            category_album_path = os.path.join(self.category_folder_path, answer)
            rename(self.category_album_path, answer)
            self.category_album_path = category_album_path
            self.category_album = answer

    def __remove_extraneous_files(self) -> None:
        """
        Remove non-song files from directory. User will be prompt before removing.
        """
        for item in listdir(self.category_album_path):
            item_path = os.path.join(self.category_album_path, item)

            # handle directory
            if os.path.isdir(item_path):
                if not SUB_ALBUM_PATTERN.fullmatch(item):
                    Announcer.ask_if_delete(item_path)

            # handle file
            else:
                title, file_extension = split_extension(item)

                if file_extension not in MUSIC_FILE_EXTENSIONS:

                    # music file with extension in uppercase
                    if file_extension.lower() in MUSIC_FILE_EXTENSIONS:
                        new_name = title + '.' + file_extension.lower()
                        new_path = os.path.join(self.category_album_path, new_name)

                        if Announcer.ask_if_rename(item_path, new_path):
                            rename(item_path, new_path)

                    else:
                        if Announcer.ask_if_delete(item_path):
                            remove(item_path)
