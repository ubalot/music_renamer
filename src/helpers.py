"""
This file contains the Renamer object. It is meant as a wrapper for some common used functions.
"""

import os
import re
from typing import List, Tuple

# DON'T CHANGE! imported this way in order to get a sharable variable.
from src.monads import listdir


def extract_dirname(path: str, level: int = 1) -> str:
    """
    Return 'level'-th depth name in path.
    'level' works in the opposite way compared to classic python way.
    """
    return path.split(os.sep)[-level]


def shrink_whitespaces(string: str) -> str:
    """ Return string without multiple spaces between words. """
    return re.sub(r' {2,}', ' ', string)


def all_content_is_dir(path: str) -> bool:
    """ Return true if all content in 'path' are directories, false otherwise. """
    file_counter = len(listdir(path))
    dir_counter = sum(1 for x in listdir(path) if os.path.isdir(os.path.join(path, x)))
    return file_counter == dir_counter


def _normalize_underscores(song: str) -> str:
    """ Remove underscores from string if they denote spaces. [it's an empirical attempt] """
    if song.count('_') > 1:
        return song.replace('_', ' ')
    return song


def _remove_name(name: str, song: str) -> str:
    """ Remove album name from song title. Only first instance of album name will be removed. """
    if name.lower() in song.lower():
        album_regex = re.compile(re.escape(name), re.IGNORECASE)
        candidate = album_regex.sub('', song, 1)

        # remove 'name' only if it is not the entire title.
        match = re.search(r'[a-zA-Z]', candidate)
        if match.start() < candidate.rfind('.'):  # check that the first letter in filename is not the extension.
            return candidate

    return song


def _remove_artist_name(artist: str, song: str) -> str:
    """ Remove artist name from song title. Only first instance of artist name will be removed. """

    def get_artist_name_variants() -> List[str]:
        # check if artist name is in song filename, then remove it
        artist_variants = [artist, 'the ' + artist]
        if ' ' in artist:
            artist_variants += ['_'.join(artist.split(' '))]
        if '.' in artist:
            artist_variants += [artist.replace('.', '')]
        artist_variants.extend([a.title() for a in artist_variants])
        return artist_variants

    for variant in get_artist_name_variants():
        if variant.lower() in song.lower():
            candidate = _remove_name(variant, song)
            if candidate != song:
                return candidate

    return song


def sanitise_title(title: str, artist: str, album: str) -> str:
    title = _normalize_underscores(title)
    title = _remove_artist_name(artist, title)
    title = _remove_name(album, title)
    return shrink_whitespaces(title)


def split_digits_and_title(song: str) -> Tuple[List[str], str]:
    """ Check if song starts with track numbers: if not, remove all previous chars. """
    digits = re.findall(r"\D?\d{1,3}\D", song, re.IGNORECASE)
    if digits:
        digits = [d.strip().replace('-', '').replace('.', '') for d in digits]
        digits = [d for d in digits if d.isnumeric()]
        if len(digits) > 0 and digits[0] and digits[0][0] != song[0]:
            track_position = song.find(digits[0][0])
            return digits, song[track_position:]
    return digits, song


def _split_first_word_at_char(words: List[str], char: str) -> List[str]:
    char_pos = words[0].find(char)
    track, first = words[0][:char_pos], words[0][char_pos + 1:]
    if first:
        words = [track, first] + words[1:]
    else:
        words = [track] + words[1:]
    return words


def _normalize_first_word(words: List[str]) -> List[str]:
    for char in ['-', '_', '.']:
        if char in words[0]:
            words = _split_first_word_at_char(words, char)
    return words


def _normalize_second_word(words: List[str]) -> List[str]:
    if len(words) > 1:
        if words[1] == '-':
            words = [words[0]] + words[2:]

        if words[1][0] == '.':
            words[1] = words[1][1:]
    return words


def split_song_filename(words: List[str]) -> Tuple[str, List[str], str]:
    _words = _normalize_second_word(_normalize_first_word(words))

    ext_point = _words[-1].rfind('.')
    extension = _words[-1][ext_point + 1:].lower()
    _words[-1] = _words[-1][:ext_point]

    track = _words[0]
    title_words = _words[1:]
    return track, title_words, extension


def split_extension(filename: str) -> Tuple[str, str]:
    last_dot = filename.rfind('.')
    return filename[:last_dot], filename[last_dot + 1:]  # char '.' will not be in any of the strings.


def _trim_whitespaces(string: str) -> str:
    while string and string[0] == ' ':
        string = string[1:]
    while string and string[-1:] == ' ':
        string = string[:-1]
    return string


def normalize_title(title: str) -> str:
    if not title:
        return ''

    if title[0] == '-':
        title = title[1:]

    return _normalize_title_case(_trim_whitespaces(shrink_whitespaces(title)))


def _normalize_title_case(title: str) -> str:
    """
    Title must be in its final form so that this function can lowercase all words that are not nouns and words
    that come after a "'".
    """
    always_lower_case_words = {'a', 'an', 'and', 'are', 'at', 'by', 'feat.', 'for', 'ft.', 'in', 'of', 'on', 'or',
                               'the', 'to', 'vers.', 'with', "'n'"}

    sanitised_title = []

    for i, word in enumerate(title.split(' ')):
        tmp_word = word

        # word after "'" must be lowercase.
        if "'" in word and word[-1] != "'":
            idx = word.find("'") + 1
            tmp_word = word[:idx] + word[idx].lower() + (word[idx + 1:] if idx < len(word) else '')

        if i == 0:
            tmp_word = word[0].upper()
            if len(word) > 1:
                tmp_word += word[1:]
        else: # checks that are valid only after the first word.
            # Words that are not nouns must be lowercase, except if they are the first word in title.
            if word.lower() in always_lower_case_words:
                tmp_word = word.lower()

            # Check words inside parenthesis (usually featuring and versions).
            if word[0] == '(' and word[-1] == ')':
                tmp_word = ''.join(w.lower if w.lower() in always_lower_case_words else w for w in word.split(' '))

        sanitised_title.append(tmp_word)

    return ' '.join(sanitised_title)

def sleep(ms: int = 30) -> None:
    os.system(f'sleep {ms}')