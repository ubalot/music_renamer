import os
from typing import Any, List, Callable

from src.announcer import Announcer


def exception_handler(default: Any = None) -> Callable:
    """ Set default argument to be returned. """

    def decorator(func: Callable) -> Callable:
        """ Real decorator. """

        def inner(*args) -> Any:
            """ Apply decorated function to its arguments and return the result, otherwise return default value. """
            try:
                return func(*args)
            except (FileNotFoundError, OSError) as err:
                Announcer.error(' '.join([str(type(err)), str(err), '\n']))
                return default

        return inner

    return decorator


@exception_handler([])
def listdir(path: str) -> List[str]:
    return os.listdir(path)


@exception_handler()
def rename(src_path: str, dst_path: str) -> None:
    os.rename(src_path, dst_path)
    Announcer.info(f"Renamed to '{dst_path}'\n")


@exception_handler()
def remove(file_path: str) -> None:
    os.remove(file_path)
    Announcer.info(f"Removed '{file_path}'\n")

