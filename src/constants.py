import re


def extend_to_lowercase(_set: set) -> set:
    """ Return input set plus values in lowercase format. """
    return _set | {x.lower() for x in _set}


def extend_to_uppercase(_set: set) -> set:
    """ Return input set plus values in uppercase format. """
    return _set | {x.upper() for x in _set}


def extend_to_title(_set: set) -> set:
    """ Return input set plus values in title format. """
    return _set | {x.title() for x in _set}


def extend(_set: set) -> set:
    """ Return input set plus values in upper, lowercase and title format. """
    return extend_to_title(extend_to_uppercase(extend_to_lowercase(_set)))


MUSIC_FILE_EXTENSIONS = extend({'mp3', 'flac', 'ogg', 'm4a', 'mpc', 'wma'})


# Auxiliary variables for compiled patterns (see below)

SPECIAL_CHARS_PATTERN = r"-–°’´`'!\?=;,.%&#∞\$\{\}\[\]\(\)\|\+\-"

CHAR_PATTERN = r"[(\w)" + SPECIAL_CHARS_PATTERN + "]"

WORD_PATTERN = CHAR_PATTERN + r"+"

WORDS_PATTERN = r"(" + WORD_PATTERN + r"( )?)+"


# Compiled patterns

CATEGORY_FOLDER_PATTERN = re.compile(
    r"((Live(s)?)|(Singles)|(Raccolt[ae])|(Greatest Hits)|((The )?Best (o|O)f))( \(?\d{2}-?\d{2}\)?)?")

ALBUM_PATTERN = re.compile(r"".join([r"\(\d{4}\) (", WORD_PATTERN, r"( )?)+"]))

SUB_ALBUM_PATTERN = re.compile(r"".join([
    r"((cd)|(Cd)|(CD)|(((vol)|(Vol)|(VOL))\.?)|(Disc)|(Greatest Hits))( )?[\dI]*((: )?",
    WORDS_PATTERN,
    r")?"]))

SONG_PATTERN = re.compile(r"".join([r"\d{1,3} - ",
                                    WORDS_PATTERN,
                                    r"\.(",
                                    r'|'.join(MUSIC_FILE_EXTENSIONS),
                                    r")"]))
