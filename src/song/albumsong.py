import os
import re
import shutil

from src.announcer import Announcer
from src.constants import SONG_PATTERN
from src.helpers import normalize_title, split_digits_and_title, split_song_filename, sanitise_title, sleep
from src.monads import rename


class AlbumSong:
    def __init__(self, artists_folder: str, artist: str, album: str, song: str) -> None:
        self.root = artists_folder

        self.artist = artist
        self.artist_path = os.path.join(self.root, artist)

        self.album = album
        self.album_path = os.path.join(self.artist_path, album)

        self.song = song
        self.song_path = os.path.join(self.album_path, song)

    def scan(self) -> None:
        """ Convert song name. """
        Announcer.info(f'scanning {self.song}\n')

        if not SONG_PATTERN.fullmatch(self.song):
            self._rename()

    def _rename(self) -> None:
        """ Rename song filename as standard song name """

        # check if song is an album (it could contains images or other files)
        if os.path.isdir(self.song_path):
            if Announcer.ask_if_delete(self.song_path):
                shutil.rmtree(self.song_path)
                Announcer.info(f"Removed  '{self.song_path}'\n")
        else:
            song = sanitise_title(self.song, self.artist, self.album)
            digits, song = split_digits_and_title(song)
            words = [w for w in re.split(r' |_', song) if w]

            if len(words) < 2 and not digits:
                Announcer.error(f"Song '{self.song_path}' doesn't have a track")
                answer = Announcer.ask_input(self.song)
                if answer:
                    song_path = os.path.join(self.album_path, answer)
                    rename(self.song_path, song_path)
            else:
                track, title_words, extension = split_song_filename(words)

                digits = re.findall(r"\d{1,3}", track)
                if digits:
                    track = digits[0]
                    normalized_title = normalize_title(' '.join(title_words))
                    song = track + ' - ' + normalized_title + '.' + extension
                    song_path = os.path.join(self.album_path, song)

                    if Announcer.ask_if_rename(self.song, song):
                        rename(self.song_path, song_path)
                else:
                    Announcer.error(f"Song '{self.song_path}' doesn't have a track")
                    answer = Announcer.ask_input(self.song)
                    if answer:
                        song_path = os.path.join(self.album_path, answer)
                        rename(self.song_path, song_path)
                    else:
                        Announcer.error(f"Song '{self.song_path}' doesn't match song regex.")
                        sleep()
