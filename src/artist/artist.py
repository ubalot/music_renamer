"""

"""

import os

from src.album.album import Album
from src.album.subalbum import SubAlbum
from src.announcer import Announcer
from src.categoryalbum.categoryfolder import CategoryFolder
from src.constants import CATEGORY_FOLDER_PATTERN
from src.monads import listdir


class Artist:
    """ """
    def __init__(self, artists_folder: str, artist: str) -> None:
        self.root = artists_folder

        self.artist_path = os.path.join(self.root, artist)
        self.artist = artist

        self.category_folders, self.albums, self.spare_songs = [], [], []

    def make_album(self, album: str) -> Album:
        """ """
        return Album(self.root, self.artist_path, album)

    def make_sub_album(self, album: str, sub_album: str) -> SubAlbum:
        """ """
        return SubAlbum(self.root, self.artist_path, album, sub_album)

    def make_category_folder(self, category_folder: str) -> CategoryFolder:
        """ """
        return CategoryFolder(self.root, self.artist_path, category_folder)

    def scan(self) -> None:
        """ """
        Announcer.info(f'scanning {self.artist} ...\n')

        def isdir(_dir: str) -> bool:
            """ """
            return os.path.isdir(os.path.join(self.artist_path, _dir))

        spare_songs = [f for f in listdir(self.artist_path) if not isdir(f)]
        directories = [f for f in listdir(self.artist_path) if isdir(f)]

        category_folders, albums = [], []
        for directory in directories:
            if CATEGORY_FOLDER_PATTERN.match(directory):
                category_folders.append(directory)
            else:
                albums.append(directory)

        self.category_folders, self.albums, self.spare_songs = category_folders, albums, spare_songs
