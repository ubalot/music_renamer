"""
Announcer is an object that controls output messages.
"""


def blue(text: str) -> str:
    return ''.join(['\033[94m', text, '\033[0m'])


def green(text: str) -> str:
    return ''.join(['\033[92m', text, '\033[0m'])


def red(text: str) -> str:
    return ''.join(['\033[91m', text, '\033[0m'])


def yellow(text: str) -> str:
    return ''.join(['\033[93m', text, '\033[0m'])


class Announcer:
    """
    Handles messages to user: logs, warnings, errors, questions.
    """

    @staticmethod
    def info(text: str) -> None:
        """ Print a info in green text """

        print(green('Info: ' + text))

    @staticmethod
    def warning(text: str) -> None:
        """ Print an error message in red color """

        print(yellow('Warning: ' + text))

    @staticmethod
    def error(text: str) -> None:
        """ Print an error message in red color """

        print(red('Error: ' + text))

    @staticmethod
    def message(text: str) -> None:
        """ Print an error message in blue color."""

        print(blue('\n' + text))

    @staticmethod
    def ask_if_delete(file_path: str) -> bool:
        """ Ask if remove file. accept only 'y' e 'n' as answer """

        question = f"Do you want to DELETE '{file_path}' ? [y/n]: "

        answer = input(red(question))

        if answer.lower() in {'y', 'yes'}:
            return True

        if answer.lower() in {'n', 'no'}:
            return False

        return Announcer.ask_if_delete(file_path)

    @staticmethod
    def ask_if_rename(old_path: str, new_path: str) -> bool:
        """ Ask if _rename file. accept only 'y' e 'n' as answer """

        if old_path == new_path:
            return False

        question = f"Do you want to RENAME\n\t{old_path}\ninto\n\t{new_path}\n?[y/n]: "

        answer = input(blue(question))

        if answer.lower() in {'y', 'yes'}:
            return True

        if answer.lower() in {'n', 'no'}:
            return False

        return Announcer.ask_if_rename(old_path, new_path)

    @staticmethod
    def ask_input(filename) -> bool:
        """
        Ask the user a free answer question. User can answer 'n' to avoid the
        question. If no answer was given, the question will be prompt again.
        """

        question = f"How do you want to RENAME '{filename}'? [n]: "

        answer = input(yellow(question))

        if not answer:
            return Announcer.ask_input(filename)

        if answer.lower() in {'n', 'no'}:
            return False

        return answer
