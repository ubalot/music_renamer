#!/usr/bin/env python3

""" Script to _rename all music files. """

import os
import sys

from src.announcer import Announcer
from src.artist.artist import Artist
from src.helpers import sleep
from src.monads import listdir


def main(artists_folder: str) -> None:
    """ Main application entry point. """
    Announcer.info(f'scanning {artists_folder} ...\n')

    for artist_name in listdir(artists_folder):

        artist = Artist(artists_folder, artist_name)
        artist.scan()

        for category_album_name in artist.category_folders:
            category_folder = artist.make_category_folder(category_album_name)
            category_folder.scan()
            del category_folder

        for album_name in artist.albums:
            album = artist.make_album(album_name)
            album.scan()
            del album

        for song in artist.spare_songs:
            Announcer.error(f"Create a 'Singles' directory and move '{song}' in there.")
            sleep()

        del artist

    Announcer.info(f'scan of {artists_folder} ended.\n')


if __name__ == '__main__':
    ARTISTS_FOLDER = sys.argv[1] if len(sys.argv) == 2 else '/tmp/test_folder'

    if not os.path.exists(ARTISTS_FOLDER):
        print(f"Folder {ARTISTS_FOLDER} doesn't exists.\nMaybe you should mount it?")
        sys.exit(2)

    try:
        main(ARTISTS_FOLDER)
    except KeyboardInterrupt:
        Announcer.message('Execution stopped by user.\n')

    sys.exit(0)
